//
//  ViewController.h
//  TestAppWork
//
//  Created by user123369 on 8/29/17.
//  Copyright © 2017 user123369. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum
{
    animationJump,
    animationRemove,
    animationAppear
}animationType;

@interface ViewController : UIViewController


@property (weak, nonatomic) IBOutlet UIView *bigView;
@property (weak, nonatomic) IBOutlet UIButton *buttonOutlet;

- (IBAction)buttonRemoveAction:(id)sender;

@end

