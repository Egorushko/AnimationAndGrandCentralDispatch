//
//  ViewController.m
//  TestAppWork
//
//  Created by user123369 on 8/29/17.
//  Copyright © 2017 user123369. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (assign, nonatomic) CGFloat dimentionOfBigView;
@property (assign, nonatomic) CGFloat dimentionOfSubView;
@property (assign, nonatomic) NSInteger animationFlag;
@property (weak, nonatomic) UILabel* lableView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dimentionOfBigView = 130.f;
    self.dimentionOfSubView = 40.f;
    self.animationFlag = -1;
    
    [self.bigView layoutIfNeeded];
    CGFloat bigRectX = CGRectGetMinX(self.bigView.bounds)+10;
    CGFloat bigRectY = CGRectGetMinY(self.bigView.bounds)+10;
    CGFloat bigWidth = CGRectGetWidth(self.bigView.bounds)-20;
    CGFloat bigHeight = CGRectGetHeight(self.bigView.bounds)-20;
    //CGFloat bigWidth = self.dimentionOfBigView;
    //CGFloat bigHeight = self.dimentionOfBigView;
    
    CGRect bigRect = CGRectMake(bigRectX, bigRectY, bigWidth, bigHeight);
    NSLog(@"bigRect %@", NSStringFromCGRect(bigRect));
    UIView* bigV = [[UIView alloc] initWithFrame:bigRect];
    bigV.backgroundColor = [UIColor blackColor];
    [self.bigView addSubview:bigV];
    CGRect bounds = bigV.bounds;
    CGRect lableRect = CGRectMake((CGRectGetWidth(bounds)-80)/2+10,
                                  (CGRectGetHeight(bounds)-50)/2+10, 80, 50);
    UILabel* lable = [[UILabel alloc] initWithFrame:lableRect];
    NSLog(@"lableRect %@", NSStringFromCGRect(lableRect));
    lable.text = @"Hi";
    lable.textColor = [UIColor redColor];
    //lable.backgroundColor = [UIColor yellowColor];
    [lable setTextAlignment:NSTextAlignmentCenter];
    self.lableView = lable;
    [self.bigView addSubview:lable];
    
    self.buttonOutlet.backgroundColor = [UIColor yellowColor];
    
    
    
    
}


-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self getNumbers];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - My Functions

-(void) getNumbers
{
    __weak ViewController* weakSelf = self;
    
    dispatch_queue_t queue = dispatch_queue_create("EAQueue", DISPATCH_QUEUE_SERIAL);
    dispatch_async(queue, ^{
        __block NSInteger i = 0;
        while (true) {
            int count=0;
            
            i++;
            NSString* str = [NSString       stringWithFormat:@"%ld", i];
            NSLog(@"i = %ld", i);
            if(i==1)
            {
                
                //NSLog(@"i==1");
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    weakSelf.lableView.text = str;
                });
            }
            else{
                for (NSInteger j=2; j<i; j++) {
                    NSLog(@"j = %ld", j);
                    if(i%j==0)
                    {
                        count=1;
                        break;
                    }
                    if(count==0)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            weakSelf.lableView.text = str;
                        });
                    }
                }
                
            }
        }
    });
}

#pragma mark - Actions

- (IBAction)buttonRemoveAction:(id)sender {
    [self jumpView:self.bigView
           scaleUp:1.2
         scaleDown:0.05
     animationType:animationRemove];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self jumpView:self.bigView
               scaleUp:1.2
             scaleDown:1/1.2
         animationType:animationAppear];
    });
    //self.bigView.hidden = true;
}

#pragma mark - visualEffects

-(void) jumpView: (UIView*) viewForEffect
         scaleUp: (CGFloat) scaleUp
       scaleDown: (CGFloat) scaleDown
   animationType: (animationType) animationType
{
    NSArray* amimationArray=nil;
    if(animationType==animationRemove)
    {
        amimationArray = @[@"1",@"2",@"3",@"end"];
    }
    else if (animationType == animationJump)
    {
        amimationArray = @[@"1",@"2",@"end"];
    }
    else if(animationType==animationAppear)
    {
        amimationArray = @[@"4",@"1",@"2",@"end"];
    }
    self.animationFlag+=1;
    NSLog(@"%@", amimationArray[self.animationFlag]);
    if(![amimationArray[self.animationFlag] isEqualToString: @"end"])
    {
    [UIView animateWithDuration:0.4
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut animations:^{
                            
                            if([amimationArray[self.animationFlag] isEqualToString: @"1"])
                            {
                                
                                CGAffineTransform scale = CGAffineTransformMakeScale(scaleUp, scaleUp);
                                viewForEffect.transform =scale;
                            }
                            else if ([amimationArray[self.animationFlag] isEqualToString: @"2"])
                            {
                                
                                CGAffineTransform scale = CGAffineTransformMakeScale(scaleDown, scaleDown);
                                viewForEffect.transform =scale;
                            }
                            else if ([amimationArray[self.animationFlag] isEqualToString: @"3"])
                            {
                                self.bigView.hidden = true;
                            }
                            else if ([amimationArray[self.animationFlag] isEqualToString: @"4"])
                            {
                                self.bigView.hidden = false;
                            }
                            

                            
                        } completion:^(BOOL finished) {
                            [self jumpView:viewForEffect
                                   scaleUp:scaleUp
                                 scaleDown:scaleDown
                             animationType:animationType];
                        }];
    
    }
    else
    {
        self.animationFlag=-1;
    }
    
}

@end
